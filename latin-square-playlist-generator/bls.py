__all__ = ['bls']

# Don't look below, you will not understand this Python code :) I don't.

from js2py.pyjs import *
# setting scope
var = Scope( JS_BUILTINS )
set_global_object(var)

# Code follows:
var.registers(['balancedLatinSquare'])
@Js
def PyJsHoisted_balancedLatinSquare_(array, participantId, this, arguments, var=var):
    var = Scope({'array':array, 'participantId':participantId, 'this':this, 'arguments':arguments}, var)
    var.registers(['array', 'idx', 'participantId', 'i', 'val', 'h', 'j'])
    var.put('result', Js([]))
    #for JS loop
    var.put('i', Js(0.0))
    var.put('j', Js(0.0))
    var.put('h', Js(0.0))
    while (var.get('i')<var.get('array').get('length')):
        try:
            var.put('val', Js(0.0))
            if ((var.get('i')<Js(2.0)) or ((var.get('i')%Js(2.0))!=Js(0.0))):
                var.put('val', (var.put('j',Js(var.get('j').to_number())+Js(1))-Js(1)))
            else:
                var.put('val', ((var.get('array').get('length')-var.get('h'))-Js(1.0)))
                var.put('h',Js(var.get('h').to_number())+Js(1))
            var.put('idx', ((var.get('val')+var.get('participantId'))%var.get('array').get('length')))
            var.get('result').callprop('push', var.get('array').get(var.get('idx')))
        finally:
                var.put('i',Js(var.get('i').to_number())+Js(1))
    if (((var.get('array').get('length')%Js(2.0))!=Js(0.0)) and ((var.get('participantId')%Js(2.0))!=Js(0.0))):
        var.put('result', var.get('result').callprop('reverse'))
    return var.get('result')
PyJsHoisted_balancedLatinSquare_.func_name = 'balancedLatinSquare'
var.put('balancedLatinSquare', PyJsHoisted_balancedLatinSquare_)
pass
pass


# Add lib to the module scope
bls = var.to_python()