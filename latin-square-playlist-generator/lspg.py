from io import TextIOWrapper
import itertools
from functools import reduce
from enum import Enum
from dataclasses import dataclass
from pathlib import Path

fileFormat = "mov"


class Design(Enum):
    between = 0
    within = 1


@dataclass
class Factor:
    levels: list[chr]
    type: Design


def main():
    materialLevels = ['S', 'F', 'L', 'M']
    shapeLevels = ['U', 'D', 'W', 'T', 'E']
    frequencyLevels = ['H', 'L', 'N']
    amplitudeLevels = ['F', 'H']

    material = Factor(materialLevels, Design.between)
    shape = Factor(shapeLevels, Design.within)
    frequency = Factor(frequencyLevels, Design.within)
    amplitude = Factor(amplitudeLevels, Design.within)

    factors = (shape,frequency,amplitude)

    groups = createGroups(*factors)

    conditions = groupedConditions(*factors, groups=groups)
    # conditions = conditionsFor(material, shape, amplitude)
    # conditions = conditionsFor(material, shape, frequency)
    # conditions = conditionsFor(frequency, amplitude)
    # conditions = conditionsFor(material)
    print(conditions)
    latinSquares = createLatinSquares(conditions)

    for groupIndex, group in enumerate(groups):
        groupName = concatinateTuple(group)
        groupFolder = Path("output/" + groupName + "/")
        latinSquare = latinSquares[groupIndex]
        groupFolder.mkdir(parents=True, exist_ok=True)
        toCSV(latinSquare, groupFolder)
        createPlaylists(latinSquare, groupFolder)


def groupedConditions(*factors: Factor, groups: list[tuple]) -> list[list[str]]:
    groupedConditions = []
    for group in groups:
        groupFactors = []
        nextBetweenIndex = 0
        for factor in factors:
            if factor.type == Design.between:
                groupFactors.append(
                    Factor(list(group[nextBetweenIndex]), Design.between))
                nextBetweenIndex += 1
            if factor.type == Design.within:
                groupFactors.append(factor)
        groupConditions = conditions(*groupFactors)
        groupedConditions.append(groupConditions)
    return groupedConditions


def createGroups(*factors: Factor):
    withinFactors = []
    betweenFactors = []
    for factor in factors:
        if factor.type == Design.between:
            betweenFactors.append(factor)
        if factor.type == Design.within:
            withinFactors.append(factor)

    betweenLevels = list(map(lambda factor: factor.levels, betweenFactors))
    groups = list(itertools.product(*betweenLevels))
    return groups


def conditions(*factors: Factor):
    levels = list(map(lambda factor: factor.levels, factors))
    conditionTuple = list(itertools.product(*levels))
    return list(map(concatinateTuple, conditionTuple))


def concatinateTuple(stringTuple: tuple):
    def concatinate(s, t): return s+t
    return reduce(concatinate, stringTuple, "")


def createLatinSquares(groupedConditions: list[list[str]]):
    latinSqaures = []
    for group in groupedConditions:
        groupLatinSquare = latinSquareFor(group)
        latinSqaures.append(groupLatinSquare)
    return latinSqaures


def latinSquareFor(conditons: list[str]):
    square = []
    for idx, _ in enumerate(conditons):
        square.append(balancedLatinSquare(conditons, idx))
    return square

# Source of balancedLatinSquare function: https://cs.uwaterloo.ca/~dmasson/tools/latin_square/


def balancedLatinSquare(conditions: list[any], participantId) -> list[any]:
    result = []
    # Based on "Bradley, J. V. Complete counterbalancing of immediate sequential effects in a Latin square design. J. Amer. Statist. Ass.,.1958, 53, 525-528. "
    j = 0
    h = 0
    i = 0
    for condition in conditions:
        value = 0
        currentIterationLessThanTwoOrOdd = i < 2 or i % 2 != 0
        if currentIterationLessThanTwoOrOdd:
            value = j
            j += 1
        else:
            value = len(conditions) - h - 1
            h += 1
        index = (value + participantId) % len(conditions)
        result.append(conditions[index])
        i += 1

    if (len(conditions) % 2 != 0 and participantId % 2 != 0):
        result.reverse()

    return result


def toCSV(square: list[list[str]], folder: Path):
    groupFile = folder / "latin-sqaure.csv"
    with open(groupFile, "w") as file:
        firstRow = square[0]
        file.write("Playlist")
        for conditionNumber in range(1, len(firstRow) + 1):
            file.write(",")
            file.write(str(conditionNumber))
        file.write("\n")
        for rowIndex, row in enumerate(square):
            file.write(str(rowIndex))
            for column in row:
                file.write(",")
                file.write(column)
            file.write("\n")


def createPlaylists(latinSquare: list[str], folder: Path):
    for index, row in enumerate(latinSquare):
        playlistFileName = str(index) + ".m3u"
        playlistFile = folder / playlistFileName
        with open(playlistFile, "w") as f:
            writePlaylistHeader(f)
            for condition in row:
                writePlaylistEntry(condition, f)


def writePlaylistHeader(file: TextIOWrapper):
    file.write("#EXTM3U")
    file.write("\n")


def writePlaylistEntry(condition: str, file: TextIOWrapper):
    file.write("#EXTINF:0," + condition)
    file.write("\n")
    location = "../../videos/" + condition + "." + fileFormat
    file.write(location)
    file.write("\n")


if __name__ == "__main__":
    main()
