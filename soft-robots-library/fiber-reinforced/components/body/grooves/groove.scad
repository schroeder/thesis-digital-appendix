module groove(length = undef){
    // cylinder(h = groove_length, d = groove_thickness);
    length = length == undef ? groove_length : length;
    translate([0, -groove_thickness/2, 0])
    cube(size=[groove_thickness, groove_thickness, length], center=false);
}

module inclined_groove(clockwise_inclination=true){
    counter_clockwise_factor = clockwise_inclination ? 1 : -1;
    rotate([ 0, groove_inclination * counter_clockwise_factor, 0 ])
    groove();
}